//
//  PlacesModel.swift
//  FlightDemoProject
//
//  Created by Mary Tany on 23.07.2021.
//

/// Модель данных места, куда можно полететь (аэропорт/город/страна)
struct Place: Decodable {

	struct Coordinates: Decodable {
		let lat: Double
		let lon: Double
	}

	// MARK: общие данные

	/// город и страна локации
	let placeName: String?
	/// координаты локации
	let coordinates: [Double]?
	/// аббревиатура локации
	let iata: String?
	/// координаты локации
	let location: Coordinates?
	/// аббревиатура страны
	let countryIata: String?
	/// количество поисков
	let searchesCount: Int?
	/// слова, по которым можно найти данную локацию
	let indexStrings: [String]?

	// MARK: данные только для аэропорта

	/// аббревиатура города, в котором находится аэропорт
	let cityIata: String?
	/// полное название аэропорта
	let airportName: String?
	/// падежи назввания города, в котором находится аэропорт
	let cityCases: Dictionary<String, String>?

	// MARK: данные только для города/страны, имеющей несколько аэропортов

	/// падежи для города/страны, имеющей несколько аэропортов
	let cases: Dictionary<String, String>?

	enum CodingKeys: String, CodingKey {
		case placeName = "name"
		case coordinates
		case iata
		case location
		case countryIata = "country_iata"
		case searchesCount = "searches_count"
		case indexStrings = "index_strings"
		case cityIata = "city_iata"
		case airportName = "airport_name"
		case cityCases = "city_cases"
		case cases
	}
}

// MARK: - CustomStringConvertible

extension Place: CustomStringConvertible {
	var description: String {
		var text = ""
		if let placeIata = iata {
			text.append("(\(placeIata))")
		}
		if let specificAirport = airportName {
			let airport = "аэропорт " + specificAirport
			text.append(text.isEmpty ? airport : " " + airport)
		}
		if let place = placeName {
			text.append(text.isEmpty ? place : " " + place)
		}
		return text
	}
}
