//
//  PlacesService.swift
//  FlightDemoProject
//
//  Created by Mary Tany on 23.07.2021.
//

import Foundation

/// Сервис по загрузке списка мест, куда можно полететь
final class PlacesService {

	/// Загрузить список мест, подходящих под запрос
	/// - Parameters:
	///   - inputText: введенный текст для поиска мест
	///   - completion: комплишн блок
	func loadPlaces(inputText: String, completion: @escaping (Result<[Place], Error>) -> Void) {
		let urlString = "https://places.aviasales.ru/places?term=\(inputText)&locale=ru"
		loadJson(fromURLString: urlString) { [weak self] result in
			switch result {
				case .success(let data):
					if let parseResult = self?.parse(jsonData: data) {
						completion(.success(parseResult))
					}
				case .failure(let error):
					print(error)
					completion(.failure(error))
			}
		}
	}

	/// Загрузка локального файла (мока)
	/// - Parameter name: название файла
	/// - Returns: данные
	private func readLocalFile(forName name: String) -> Data? {
		do {
			if let bundlePath = Bundle.main.path(forResource: name, ofType: "json"),
			   let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
				return jsonData
			}
		} catch {
			print(error)
		}
		return nil
	}

	/// Парсинг данных json
	/// - Parameter jsonData: полученные данные
	/// - Returns: модель данных
	private func parse(jsonData: Data) -> [Place]? {
		do {
			let decodedData = try JSONDecoder().decode([Place].self,
													   from: jsonData)
			return decodedData
		} catch {
			print("decode error")
			return nil
		}
	}

	/// Загрузить json
	/// - Parameters:
	///   - urlString: адрес для загрузки
	///   - completion: комплишн блок
	private func loadJson(fromURLString urlString: String,
						  completion: @escaping (Result<Data, Error>) -> Void) {
		guard let encoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
			  let url = URL(string: encoded) else { return }

		let urlSession = URLSession(configuration: .default).dataTask(with: url) { (data, response, error) in
			if let error = error {
				completion(.failure(error))
			}

			if let data = data {
				completion(.success(data))
			}
		}
		urlSession.resume()
	}
}
