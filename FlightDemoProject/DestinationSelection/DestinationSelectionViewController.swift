//
//  DestinationSelectionViewController.swift
//  FlightDemoProject
//
//  Created by Mary Tany on 22.07.2021.
//

import UIKit

/// Контроллер экрана выбора пункта назначения, куда мы полетим
class DestinationSelectionViewController: UIViewController {

	private struct Constants {
		static let defaultIndent: CGFloat = 20
		static let customCellIdentifier: String = "MyCell"
	}

	/// сервис для загрузки данных
	private var placesService = PlacesService()
	/// массив загруженных мест, подходящих под поисковой запрос
	private var loadedPlaces = [Place]() {
		didSet {
			DispatchQueue.main.async {
				self.placesTable.isHidden = self.loadedPlaces.isEmpty
				self.placesTable.reloadData()
			}
		}
	}
	
	private lazy var titleLabel: UILabel = {
		let label = UILabel()
		label.text = "Привет, друг!"
		label.textAlignment = .center
		label.font = UIFont(name: "Apple SD Gothic Neo Heavy", size: 20)
		label.translatesAutoresizingMaskIntoConstraints = false
		return label
	}()
	
	private lazy var chooseLabel: UILabel = {
		let label = UILabel()
		label.text = "Напиши аэропорт, город или страну, куда мы полетим из Санкт-Петербурга.\n\nИли отправь пустой запрос, чтобы увидеть нашу подборку вариантов"
		label.textAlignment = .center
		label.font = UIFont(name: "Apple SD Gothic Neo Heavy", size: 20)
		label.translatesAutoresizingMaskIntoConstraints = false
		label.numberOfLines = 0
		return label
	}()
	
	private lazy var cityField: UITextField = {
		let field = UITextField()
		field.translatesAutoresizingMaskIntoConstraints = false
		field.textAlignment = .center
		field.backgroundColor = .white
		field.delegate = self
		field.placeholder = "Куда летим?"
		return field
	}()

	private lazy var searchButton: UIButton = {
		let button = UIButton()
		button.translatesAutoresizingMaskIntoConstraints = false
		button.setTitle("🔍", for: .normal)
		button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
		button.backgroundColor = .white
		button.layer.cornerRadius = 5
		return button
	}()

	private lazy var placesTable: UITableView = {
		let table = UITableView()
		table.register(UITableViewCell.self, forCellReuseIdentifier: Constants.customCellIdentifier)
		table.delegate = self
		table.dataSource = self
		table.translatesAutoresizingMaskIntoConstraints = false
		table.backgroundColor = .clear
		table.tableFooterView = UIView(frame: .zero)
		return table
	}()
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.setNavigationBarHidden(true, animated: false)
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		setupView()
	}

	private func setupView() {
		let backgroundImage = UIImage(named: "paperBackground")
		view.addSubview(UIImageView(image: backgroundImage))
		view.addSubview(titleLabel)
		view.addSubview(chooseLabel)
		view.addSubview(cityField)
		view.addSubview(searchButton)
		view.addSubview(placesTable)

		NSLayoutConstraint.activate([
			titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Constants.defaultIndent),
			titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Constants.defaultIndent),
			titleLabel.bottomAnchor.constraint(equalTo: chooseLabel.topAnchor, constant: -Constants.defaultIndent),

			chooseLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
			chooseLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
			chooseLabel.bottomAnchor.constraint(equalTo: cityField.topAnchor, constant: -Constants.defaultIndent),

			cityField.leadingAnchor.constraint(equalTo: chooseLabel.leadingAnchor),
			cityField.trailingAnchor.constraint(equalTo: searchButton.leadingAnchor, constant: -5),
			cityField.heightAnchor.constraint(equalToConstant: 40),
			cityField.bottomAnchor.constraint(equalTo: placesTable.topAnchor, constant: -Constants.defaultIndent),

			searchButton.topAnchor.constraint(equalTo: cityField.topAnchor),
			searchButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Constants.defaultIndent),
			searchButton.bottomAnchor.constraint(equalTo: cityField.bottomAnchor),
			searchButton.widthAnchor.constraint(equalToConstant: 40),

			placesTable.topAnchor.constraint(equalTo: view.centerYAnchor),
			placesTable.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			placesTable.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			placesTable.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
		])

		placesTable.isHidden = true
	}
}

// MARK: - Helpers

private extension DestinationSelectionViewController {
	func loadPlaces(for requestString: String?) {
		guard let place = requestString else { return }
		placesService.loadPlaces(inputText: place, completion: { [weak self] result in
			switch result {
				case .success(let places):
					self?.loadedPlaces = places
				case .failure(_):
					let alert = UIAlertController(title: "Что-то пошло не так..",
												  message: "При загрузке данных произошла ошибка",
												  preferredStyle: .alert)
					alert.addAction(UIAlertAction(title: "ОК", style: .default, handler: nil))
					DispatchQueue.main.async {
						self?.present(alert, animated: true)
					}
			}
		})
	}

	@objc func buttonAction() {
		guard !cityField.isFirstResponder else {
			cityField.endEditing(true)
			return
		}
		loadPlaces(for: cityField.text)
	}
}

// MARK: - UITextFieldDelegate

extension DestinationSelectionViewController: UITextFieldDelegate {
	func textFieldDidEndEditing(_ textField: UITextField) {
		loadPlaces(for: textField.text)
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		cityField.resignFirstResponder()
	}
}

// MARK: - UITableViewDelegate

extension DestinationSelectionViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let targetPlace = loadedPlaces[indexPath.row]
		let flightViewController = FlightViewController(targetPlace: targetPlace)
		navigationController?.pushViewController(flightViewController, animated: true)
	}
}

// MARK: - UITableViewDataSource

extension DestinationSelectionViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return loadedPlaces.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: Constants.customCellIdentifier,
												 for: indexPath as IndexPath)
		let selectedPlace = loadedPlaces[indexPath.row]
		cell.textLabel?.text = selectedPlace.description
		cell.textLabel?.numberOfLines = 0
		return cell
	}
}
