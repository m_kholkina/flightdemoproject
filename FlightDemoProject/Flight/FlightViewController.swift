//
//  FlightViewController.swift
//  FlightDemoProject
//
//  Created by Mary Tany on 22.07.2021.
//

import UIKit
import MapKit

/// Контроллер экрана полета
final class FlightViewController: UIViewController {

	/// место, в которое мы летим
	private let targetPlace: Place
	/// координаты места, откуда стартуем (Пулково)
	private let initialCoordinates = CLLocationCoordinate2D(latitude: 59.806084, longitude: 30.3083)
	/// брендовый цвет
	private let brandColor = UIColor(red: 0.13, green: 0.59, blue: 0.95, alpha: 1.00)
	/// шаг, регулигрующий скорость полета самолетика на карте
	private var step: Int = 0
	/// флаг, говорящий была ли уже центрирована карта
	private var mapHasBeenCentered: Bool = false
	/// позиция самолета
	private var planePosition = 0
	/// направление носа самолета
	private var planeDirection: CLLocationDirection = 0

	/// самолетик
	private var plane: MKPointAnnotation = {
		let annotation = MKPointAnnotation()
		return annotation
	}()

	/// прямоугольная область карты, описывающая краевые точки маршрута
	private var mapRectToShow: MKMapRect {
		let startPoint = MKMapPoint(initialCoordinates)
		let finishPoint = MKMapPoint(targetPlaceCoordinates)

		let mapRect = MKMapRect(x: fmin(startPoint.x,finishPoint.x),
								y: fmin(startPoint.y,finishPoint.y),
								width: fabs(startPoint.x-finishPoint.x),
								height: fabs(startPoint.y-finishPoint.y))
		return mapRect
	}

	/// пин точки вылета
	private lazy var startPin: AirportAnnotation = {
		return AirportAnnotation(coordinate: initialCoordinates, iata: "LED")
	}()

	/// координаты места, в которое мы летим
	private lazy var targetPlaceCoordinates: CLLocationCoordinate2D = {
		return CLLocationCoordinate2D(latitude: targetPlace.location?.lat ?? 0,
									  longitude: targetPlace.location?.lon ?? 0)
	}()

	/// пин точки прилета
	private lazy var finishPin: AirportAnnotation = {
		return AirportAnnotation(coordinate: targetPlaceCoordinates, iata: targetPlace.iata)
	}()

	/// траектория полета
	private lazy var flightPath: MKGeodesicPolyline = {
		return MKGeodesicPolyline(coordinates: [initialCoordinates, targetPlaceCoordinates], count: 2)
	}()

	/// карта
	private lazy var mapView: MKMapView = {
		let mapView = MKMapView()
		mapView.mapType = MKMapType.standard
		mapView.delegate = self
		mapView.translatesAutoresizingMaskIntoConstraints = false
		return mapView
	}()

	/// Инициализатор
	/// - Parameter targetPlace: место, в которое мы летим
	init(targetPlace: Place) {
		self.targetPlace = targetPlace
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationController?.setNavigationBarHidden(false, animated: false)
		setupView()
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		updateViewConstraints()
		guard !mapHasBeenCentered else { return }

		mapView.zoomAndShowArea(mapRectToShow)
		mapHasBeenCentered = true
		step = max(1, lround(Double(flightPath.pointCount) * 0.03 / 10))
		movePlane()
	}

	private func setupView() {
		view.backgroundColor = .white
		view.addSubview(mapView)
		NSLayoutConstraint.activate([
			mapView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
			mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			mapView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
		])
		mapView.addAnnotations([startPin, finishPin])
		mapView.addOverlay(flightPath)
		mapView.addAnnotation(plane)
	}
}

// MARK: - Helpers

private extension FlightViewController {

	@objc func movePlane() {
		guard planePosition + step < flightPath.pointCount else { return }
		let points = flightPath.points()
		let previousPathPoint = points[planePosition]
		planePosition += step
		let nextPathPoint = points[planePosition]

		planeDirection = calculatePlaneDirection(previousPoint: previousPathPoint, nextPoint: nextPathPoint)
		plane.coordinate = nextPathPoint.coordinate
		let realdirection = CGFloat(degreesToRadians(degrees: planeDirection))
		mapView.view(for: plane)?.transform = mapView.transform.rotated(by: realdirection)
		perform(#selector(movePlane), with: nil, afterDelay: 0.03)
	}

	func calculatePlaneDirection(previousPoint: MKMapPoint,
								 nextPoint: MKMapPoint) -> CLLocationDirection {
		let x = nextPoint.x - previousPoint.x
		let y = nextPoint.y - previousPoint.y
		let degrees = radiansToDegrees(radians: atan2(y, x))
		return degrees.truncatingRemainder(dividingBy: 360)
	}

	func radiansToDegrees(radians: Double) -> Double {
		return radians * 180 / .pi
	}

	func degreesToRadians(degrees: Double) -> Double {
		return degrees * .pi / 180
	}
}

// MARK: - MKMapViewDelegate

extension FlightViewController: MKMapViewDelegate {
	
	func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
		guard !(annotation is MKUserLocation) else { return nil }

		// вью с самолетиком
		guard annotation is AirportAnnotation else {
			let planeIdentifier = "Plane"
			let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: planeIdentifier)
				?? MKAnnotationView(annotation: annotation, reuseIdentifier: planeIdentifier)

			annotationView.image = UIImage(named: "plane")
			annotationView.canShowCallout = false
			return annotationView
		}

		// вью с названием аэропорта
		let airportName: String = (annotation.title ?? "???") ?? "???"
		let identifier = "iata"
		let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) ??
			FlightMapAnnotationView(placeIata: airportName, color: brandColor.cgColor, reuseIdentifier: identifier)
		annotationView.canShowCallout = false
		return annotationView
	}
	
	func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
		guard let polyline = overlay as? MKPolyline else {
			return MKOverlayRenderer()
		}
		let renderer = MKPolylineRenderer(polyline: polyline)
		renderer.lineWidth = 3.0
		renderer.strokeColor = brandColor
		renderer.lineDashPattern = [NSNumber(value: 1),NSNumber(value:5)]
		return renderer
	}
}
