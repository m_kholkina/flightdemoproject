//
//  FlightMapAnnotationView.swift
//  FlightDemoProject
//
//  Created by Mary Tany on 25.07.2021.
//

import MapKit

/// Вью для пина аэропорта/города с несколькими аэропортами на карте
final class FlightMapAnnotationView: MKAnnotationView {

	/// аббревиатура с названием аэропорта/города с несколькими аэропортами
	private let placeIata: String

	/// лейбл с аббревиатурой
	private lazy var label: UILabel = {
		let label = UILabel()
		label.textAlignment = .center
		label.font = UIFont(name: "Apple SD Gothic Neo Heavy", size: 20)
		label.translatesAutoresizingMaskIntoConstraints = false
		label.backgroundColor = .white
		label.alpha = 0.8
		label.layer.cornerRadius = 10
		label.layer.masksToBounds = true
		label.layer.borderWidth = 2
		return label
	}()

	/// Инициализатор
	/// - Parameters:
	///   - placeIata: аббревиатура с названием аэропорта/города с несколькими аэропортами
	///   - color: цвет текста и рамки вью
	///   - reuseIdentifier: идентификатор
	init(placeIata: String, color: CGColor, reuseIdentifier: String) {
		self.placeIata = placeIata
		super.init(annotation: nil, reuseIdentifier: reuseIdentifier)
		setupView(color: color)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupView(color: CGColor) {
		label.text = placeIata
		label.layer.borderColor = color
		label.textColor = UIColor(cgColor: color)
		addSubview(label)
		NSLayoutConstraint.activate([
			label.heightAnchor.constraint(equalToConstant: 30),
			label.widthAnchor.constraint(equalToConstant: 50),
			label.centerYAnchor.constraint(equalTo: centerYAnchor),
			label.centerXAnchor.constraint(equalTo: centerXAnchor)
		])
	}
}

