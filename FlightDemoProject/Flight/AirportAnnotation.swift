//
//  AirportAnnotation.swift
//  FlightDemoProject
//
//  Created by Mary Tany on 25.07.2021.
//

import MapKit

/// Аннотация аэропорта для карты
final class AirportAnnotation: NSObject, MKAnnotation {
	var coordinate: CLLocationCoordinate2D
	var title: String?
	var subtitle: String?

	/// Инициализатор
	/// - Parameters:
	///   - coordinate: координаты аэропорта/города с несколькими аэропортами
	///   - iata: аббревиатура названия аэропорта/города с несколькими аэропортами
	init(coordinate: CLLocationCoordinate2D, iata: String?) {
		self.coordinate = coordinate
		title = iata
		subtitle = nil
	}
}
