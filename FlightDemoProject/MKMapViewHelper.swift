//
//  MKMapViewHelper.swift
//  FlightDemoProject
//
//  Created by Mary Tany on 27.07.2021.
//

import MapKit

extension MKMapView {

	/// Отзумить карту и показать заданную область (если область слишком большая и не влезает на экран, будет показан ее центр)
	/// - Parameters:
	///   - mapRect: прямоугольник карты, который нужно отобразить
	///   - edgeInset: отступ от края карты
	func zoomAndShowArea(_ mapRect: MKMapRect, withInset edgeInset: CGFloat = 50)
	{
		let edgePadding = UIEdgeInsets(top: edgeInset, left: edgeInset, bottom: edgeInset, right: edgeInset)
		let coordinateRegion = MKCoordinateRegion(mapRectThatFits(mapRect, edgePadding: edgePadding))
		setRegion(coordinateRegion, animated: false)
	}
}
