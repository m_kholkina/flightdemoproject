//
//  AppDelegate.swift
//  FlightDemoProject
//
//  Created by Mary Tany on 22.07.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication,
					 didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
		let navigation = UINavigationController()
		let startView = DestinationSelectionViewController()
		navigation.viewControllers = [startView]
		window?.rootViewController = navigation
        window?.makeKeyAndVisible()
        return true
    }
}

